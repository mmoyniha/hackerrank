/*
https://www.hackerrank.com/challenges/weather-observation-station-1/problem

Query a list of CITY and STATE from the STATION table.

Input Format

The STATION table is described as follows:
*/

SELECT CITY, STATE
    FROM STATION;
