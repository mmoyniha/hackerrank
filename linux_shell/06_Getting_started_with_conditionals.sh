#!/bin/sh

# https://www.hackerrank.com/challenges/bash-tutorials---getting-started-with-conditionals/problem

# Read in one character from the user (this may be 'Y', 'y', 'N', 'n'). If the character is 'Y' or 'y' display "YES". If the character is 'N' or 'n' display "NO". No other character will be provided as input.
# 
# Input Format
# 
# One character (this may be 'Y', 'y', 'N', 'n').
# 
# Constraints
# 
# -
# 
# Output Format
# 
# One word: either "YES" or "NO" (quotation marks excluded).
# 
# Sample Input
# 
# Sample Input 1
# 
# y  
# 
# Sample Output
# 
# Sample Output 1
# 
# YES
# 
# Explanation
# 
# -

read R

case $R in
    y|Y)
        echo YES
        ;;
    *)
        echo NO
        ;;
esac
